package handler;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import org.json.JSONObject;

import utilities.Constant;

/**
 *
 * @author Phuc Huynh
 *
 */
public class RedShiftHandler implements ActionHandler {

	// a bucket name on the AWS cloud
	private String mBucketName;
	// a file path on cloud
	private String mFilePathOnCloud;

	// a username of a database
	private String mDbUserName;
	// a user's password of a database
	private String mDbUserPassword;
	// a URL of a database
	private String mDbURL;
	// a table name of a table using to receive the data from a file on the AWS
	// S3 cloud
	private String mTableName;
	// an access privilege of a user
	private String mAim;

	public RedShiftHandler(JSONObject action) {
		// get a database information
		this.mDbUserName = action.getString(Constant.DB_USERNAME_KEY);
		this.mDbUserPassword = action.getString(Constant.DB_USER_PASSWORD_KEY);
		this.mDbURL = action.getString(Constant.DB_URL_KEY);
		this.mTableName = action.getString(Constant.DB_TABLE_NAME_KEY);
		this.mAim = action.getString(Constant.DB_AIM_KEY);
		// get a S3 information
		this.mBucketName = action.getString(Constant.BUCKET_NAME_ON_CLOUD);
		this.mFilePathOnCloud = action.getString(Constant.FILE_PATH_ON_CLOUD);
	}

	public String getBucketName() {
		return mBucketName;
	}

	public void setBucketName(String mBucketName) {
		this.mBucketName = mBucketName;
	}

	public String getFilePathOnCloud() {
		return mFilePathOnCloud;
	}

	public void setFilePathOnCloud(String mFilePathOnCloud) {
		this.mFilePathOnCloud = mFilePathOnCloud;
	}

	public String getDbUserName() {
		return mDbUserName;
	}

	public void setDbUserName(String mDbUserName) {
		this.mDbUserName = mDbUserName;
	}

	public String getDbUserPassword() {
		return mDbUserPassword;
	}

	public void setDbUserPassword(String mDbUserPassword) {
		this.mDbUserPassword = mDbUserPassword;
	}

	public String getDbURL() {
		return mDbURL;
	}

	public void setDbURL(String mDbURL) {
		this.mDbURL = mDbURL;
	}

	public String getTableName() {
		return mTableName;
	}

	public void setTableName(String mTableName) {
		this.mTableName = mTableName;
	}

	public String getAim() {
		return mAim;
	}

	public void setAim(String mAim) {
		this.mAim = mAim;
	}

	@Override
	public void act() throws Exception {
		copyS3ToRedShift(getBucketName(), getFilePathOnCloud(), getDbUserName(), getDbUserPassword(), getDbURL(),
				getTableName(), getAim());

	}

	/**
	 * Get a Redshift database connection
	 *
	 * @param dbUserName
	 *            a username of a given database
	 * @param dbUserPassword
	 *            a user's password of a given database
	 * @param dbURL
	 *            a URL of a given database
	 * @return a connection to the database
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public Connection getRedshiftDbConnection(String dbUserName, String dbUserPassword, String dbURL)
			throws ClassNotFoundException, SQLException {
		Connection conn = null;

		// dynamically load driver at runtime.
		// Redshift JDBC 4.1 driver: com.amazon.redshift.jdbc41.Driver
		// Redshift JDBC 4 driver: com.amazon.redshift.jdbc4.Driver
		Class.forName("com.amazon.redshift.jdbc4.Driver");

		// open a connection and define properties.
		Properties props = new Properties();

		// uncomment the following line if using a keystore.
		// props.setProperty("ssl", "true");
		props.setProperty("user", dbUserName);
		props.setProperty("password", dbUserPassword);
		conn = DriverManager.getConnection(dbURL, props);

		return conn;
	}

	/**
	 * Copy a file on the S3 cloud to RedShift. Notice: this function just
	 * supports *csv files (not implements Json, XML,... files yet.
	 *
	 * @param bucketName
	 *            a name of a given bucket on the AWS S3 cloud
	 * @param filePathOnCloud
	 *            a path of a file on the AWS S3 cloud that needs to be coppied
	 *            to the AWS RedShift
	 * @param client
	 *            a S3 client
	 * @param dbUserName
	 *            a username of a given database
	 * @param dbUserPassword
	 *            a user's password of a given database
	 * @param dbURL
	 *            a URL of a given database
	 * @param tableName
	 *            a table name of a table using to receive the data from a file
	 *            on the AWS S3 cloud on the AWS S3 cloud
	 * @param aim
	 *            an access privilege of a user
	 *
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public void copyS3ToRedShift(String bucketName, String filePathOnCloud, String dbUserName, String dbUserPassword,
			String dbURL, String tableName, String aim) throws IOException, ClassNotFoundException, SQLException {
		Connection conn = getRedshiftDbConnection(dbUserName, dbUserPassword, dbURL);
		Statement stmt = null;

		// initiate a statement
		stmt = conn.createStatement();

		String dataSource = bucketName + "/" + filePathOnCloud;
		// e.g. tableName = "category";
		// e.g. aim = "arn:aws:iam::651737375868:role/PhucRedshiftRole";

		String sql = String.format("COPY " + tableName + " FROM 's3://%s' " + " iam_role '%s'" + " csv;", dataSource,
				aim);

		stmt.executeUpdate(sql);
		if (stmt != null)
			stmt.close();
		if (conn != null)
			conn.close();
	}

}
