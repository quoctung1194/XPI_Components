package handler;

public interface ActionHandler {
	/**
	 * an action of handler
	 */
	public void act() throws Exception;
}
