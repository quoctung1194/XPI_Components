package utilities;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 *
 * @author Phuc Huynh
 *
 */
public class FileHandlerUtils {
	/**
	 * get a file name from its path
	 *
	 * @param filePath
	 *            a given file path
	 * @return a file name
	 */
	public static String getFileNameFromPath(String filePath) {
		String[] parts = filePath.split(Constant.SUFFIX);
		return parts[parts.length - 1];
	}

	/**
	 * Write a given file
	 *
	 * @param reader
	 *            is a input stream reader
	 * @param pathToSave
	 *            is a path on local computer to save a file e.g. D:/temp
	 * @param filePath
	 *            is a file path
	 * @throws IOException
	 */
	public static void writeFile(InputStream reader, String pathToSave, String filePath) throws IOException {
		String fileName = getFileNameFromPath(filePath);
		File file = new File(pathToSave + utilities.Constant.SUFFIX + fileName);
		OutputStream writer = new BufferedOutputStream(new FileOutputStream(file));

		int read = -1;

		while ((read = reader.read()) != -1) {
			writer.write(read);
		}

		writer.flush();
		writer.close();
		reader.close();
	}
}
