package com.magicsoftware.sdk;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.lang.reflect.Constructor;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.magicsoftware.xpi.sdk.step.IStep;
import com.magicsoftware.xpi.sdk.step.StepGeneralParams;

import handler.ActionHandler;
import utilities.Constant;

/**
 *
 * @author Phuc Huynh
 *
 */
public class IDSMagicStep implements IStep {

	public final static Logger log = Logger.getLogger("com.magicsoftware.sdk.MyFirstStep");

	@Override
	public void invoke(StepGeneralParams params) {
		log.info("invoke");
		try {

			// get client input from C# and convert it to Json object
			InputSource inputSource = new InputSource(new ByteArrayInputStream(params.getPayloadOBject()));
			JSONObject UIInput = getVariable(inputSource);
			log.info("invoke: " + UIInput.toString());

			// get user's requests
			JSONArray array = UIInput.getJSONArray(Constant.ACTIONS_KEY);
			for (int i = 0; i < array.length(); i++) {
				String className = array.getJSONObject(i).getString(Constant.CLASS_NAME_KEY);

				// call the AWS API
				callAWSAPI(array.getJSONObject(i), className);
			}
		} catch (Exception e) {
			log.info("invoke exception: " + e.getMessage());
		}
	}

	/**
	 * Call the AWS API according to user's request
	 *
	 * @param JsonAction
	 *            user request
	 * @param className
	 *            a class name of a handler
	 * @throws Exception
	 */
	public void callAWSAPI(JSONObject JsonAction, String className) throws Exception {
		log.info("invoke className: " + className);
		Class<?> aClass = Class.forName(className);
		Constructor<?> con = aClass.getConstructor(JSONObject.class);
		ActionHandler handler = (ActionHandler) con.newInstance(JsonAction);
		// do somethings according to user's requests
		handler.act();
	}

	/**
	 * get Variable from UI C#
	 *
	 * @param mappingValue
	 *            is an input source
	 * @return
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 */
	public JSONObject getVariable(InputSource mappingValue)
			throws ParserConfigurationException, SAXException, IOException {

		JSONObject variableValue = new JSONObject();
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.parse(mappingValue);
		NodeList inputNodeList = document.getElementsByTagName(Constant.TAG_NAME);

		for (int nodeCount = 0; nodeCount < inputNodeList.getLength(); nodeCount++) {

			NodeList inputChildList = inputNodeList.item(nodeCount).getChildNodes();
			for (int inputChildCnt = 0; inputChildCnt < inputChildList.getLength(); inputChildCnt++) {

				Node inputChild = inputChildList.item(inputChildCnt);
				if (inputChild.getNodeType() == Node.TEXT_NODE) {
					variableValue = new JSONObject(inputChild.getTextContent());
				}
			}
		}

		return variableValue;
	}

}
