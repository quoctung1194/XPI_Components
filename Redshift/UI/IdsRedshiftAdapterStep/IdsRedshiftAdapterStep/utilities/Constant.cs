﻿using System;

namespace SDK_REDSHIFT_IDS
{
    class Constant
    {

        /// <summary> 
        ///  class names in java Runtime
        /// </summary>
        public const String REDSHIFT_COPY_JAVA_RUNTIME_CLASS_NAME = "RedShift";

        /// <summary>
        /// some keys of Json file between Java runtime and C# UI
        /// </summary>
        // a bucket name key on the AWS cloud
        public const String BUCKET_NAME_ON_CLOUD = "bucketNameOnCloud";
        // a folder name key on the AWS cloud
        public const String FOLDER_NAME_ON_CLOUD = "folderNameOnCloud";
        // a file name key on the AWS cloud
        public const String FILE_NAME_ON_CLOUD = "fileNameOnCloud";
        // a file name key on the AWS cloud
        public const String LOCAL_FILE_TO_UPLOAD = "localFileToUpload";
        // a local path to store a downloaded file on user's computer
        public const String LOCAL_PATH_TO_STORE_FILE = "localpathToStoreFile";
        //// a file path on the AWS S3 cloud
        public const String FILE_PATH_ON_CLOUD = "filePathOnCloud";
        // an authentication key in Json input from C# to Java
        public const String AUTHENTICATION_KEY = "authenticate";
        // an actions key in Json input from C# to Java
        public const String ACTIONS_KEY = "actions";
        // a class name key in Json input from C# to Java, 
        // we use it to explicitly call a handler respectively
        public const String CLASS_NAME_KEY = "className";
        // a username of a given database
        public const String DB_USERNAME_KEY = "dbUsername";
        // a user's password of a given database
        public const String DB_USER_PASSWORD_KEY = "dbUserPassword";
        // a URL of a given database
        public const String DB_URL_KEY = "dbUrl";
        // a table name of a table using to receive the data from a file
        // on the AWS S3 cloud on the AWS S3 cloud
        public const String DB_TABLE_NAME_KEY = "tableName";
        // an access privilege of a user
        public const String DB_AIM_KEY = "aim";



        // the options
        public const String REDSHIFT_COPY = "RedShift Copy";


        public const String PARAM_NAME = "Parameter Name";
        public const String PARAM_VALUE = "Value";
        // package names of the Java Handler classes
        public const String JAVA_PACKAGE_NAME = "handler.";
        // postfix of handler classes
        public const String POSTFIX_OF_HANDLER_CLASSES = "Handler";
    }
}
