﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using Newtonsoft.Json.Linq;

namespace SDK_REDSHIFT_IDS
{
    public partial class MainUIForm : Form
    {
        /*
         * VARIABLE AREA
         */
        private int mSelectedIndexTask;
        private bool mUpdateFlg;
        private List<ArrayList> mTemplate;
        private List<ArrayList> mContainer;
        private bool configurationChanged = false;
        private bool configurationSuccess = false;

        public MainUIForm()
        {
            // initialize variables
            mSelectedIndexTask = 0;
            mUpdateFlg = true;
            mTemplate = new List<ArrayList>();
            mContainer = new List<ArrayList>();

            this.BuildTemplate();

            // init template
            InitializeComponent();
        }

        public bool isConfigurationChanged
        {
            get
            {
                return this.configurationChanged;
            }
        }

        public bool isConfigurationSuccess
        {
            get
            {
                return this.configurationSuccess;
            }
        }

        private void BtnDelClick(object sender, EventArgs e)
        {
            if (this.mSelectedIndexTask < this.mContainer.Count)
            {
                this.mContainer.RemoveAt(this.mSelectedIndexTask);
                this.GridTask.Rows.RemoveAt(this.mSelectedIndexTask);
            }
        }

        private void GridTaskCellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }

            String handlerName = GridTask.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
            this.AddRow(handlerName);
        }

        private void GridParamCellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            String handlerName = GridTask.Rows[this.mSelectedIndexTask].Cells[0].Value.ToString();

            if (this.mSelectedIndexTask >= mContainer.Count)
            {

                int paramLength = this.GetLengthParams(handlerName);
                ArrayList data = new ArrayList();
                for (var i = 0; i < paramLength; i++)
                {
                    data.Add("");
                }

                mContainer.Add(data);
            }
            else
            {
                if (!this.mUpdateFlg)
                {
                    return;
                }

                ArrayList row = (ArrayList)mContainer[mSelectedIndexTask];
                row[0] = handlerName;

                for (var i = 0; i < GridParam.RowCount; i++)
                {
                    object value = GridParam.Rows[i].Cells[1].Value;
                    if (i == row.Count - 1)
                    {
                        continue;
                    }
                    if (value == null)
                    {
                        row[i + 1] = "";
                    }
                    else
                    {
                        row[i + 1] = value;
                    }
                }
            }
        }

        private void GridTaskCurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (this.GridTask.IsCurrentCellDirty)
            {
                // This fires the cell value changed handler below
                GridTask.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }

        private void BuildTemplate()
        {
            // RedShift Template
            ArrayList rowTemplate = new ArrayList();
            rowTemplate.Add(Constant.REDSHIFT_COPY);
            rowTemplate.Add(Constant.BUCKET_NAME_ON_CLOUD);
            rowTemplate.Add(Constant.FILE_PATH_ON_CLOUD);
            rowTemplate.Add(Constant.DB_USERNAME_KEY);
            rowTemplate.Add(Constant.DB_USER_PASSWORD_KEY);
            rowTemplate.Add(Constant.DB_URL_KEY);
            rowTemplate.Add(Constant.DB_TABLE_NAME_KEY);
            rowTemplate.Add(Constant.DB_AIM_KEY);
            mTemplate.Add(rowTemplate);
        }

        private int GetLengthParams(String handlerName)
        {
            foreach (ArrayList element in this.mTemplate)
            {
                if (element[0].ToString() == handlerName)
                {
                    return element.Count;
                }
            }

            return 0;
        }

        private void AddRow(String handlerName)
        {
            // clear all current row in params
            GridParam.Rows.Clear();

            // add corrresponding rows
            ArrayList selectedRow = new ArrayList();
            for (int i = 0; i < mTemplate.Count; i++)
            {
                ArrayList row = mTemplate[i];
                string name = row[0].ToString();

                if (name == handlerName)
                {
                    selectedRow = row;
                    break;
                }
            }

            for (int i = 1; i < selectedRow.Count; i++)
            {
                var newRowIndex = GridParam.Rows.Add();
                GridParam.Rows[newRowIndex].Cells[0].Value = selectedRow[i].ToString();
            }
        }

        private void GridTaskRowEnter(object sender, DataGridViewCellEventArgs e)
        {
            this.mSelectedIndexTask = e.RowIndex;
            if (GridTask.Rows[e.RowIndex].Cells[0].Value == null)
            {
                GridParam.Rows.Clear();
                return;
            }

            // display into UI
            String handlerName = GridTask.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
            this.mUpdateFlg = false;
            this.AddRow(handlerName);

            for (int i = 0; i < GridParam.RowCount; i++)
            {
                ArrayList rowData = this.mContainer[e.RowIndex];
                GridParam.Rows[i].Cells[1].Value = rowData[i + 1];
            }

            this.mUpdateFlg = true;
        }

        private void BtnOKClick(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
            DialogResult result = folderBrowserDialog.ShowDialog();
            JObject container = new JObject();

            //create actions
            JArray actions = new JArray();

            // adjust a class name
            foreach (ArrayList arrayList in mContainer)
            {
                String actionName = arrayList[0].ToString();
                String adjustedClassName = actionName;
                switch (actionName)
                {
                    case Constant.REDSHIFT_COPY:
                        adjustedClassName = Constant.REDSHIFT_COPY_JAVA_RUNTIME_CLASS_NAME;
                        break;
                }

                JObject item = new JObject();
                item.Add(new JProperty(Constant.CLASS_NAME_KEY, Constant.JAVA_PACKAGE_NAME + adjustedClassName + Constant.POSTFIX_OF_HANDLER_CLASSES));
                for (int i = 1; i < arrayList.Count; i++)
                {
                    String paramName = this.getParamNameByIndex(actionName, i);
                    item.Add(new JProperty(paramName, arrayList[i].ToString()));
                }
                actions.Add(item);
            }
            container[Constant.ACTIONS_KEY] = actions;
            if (result == DialogResult.OK)
            {
                this.ExportResult(folderBrowserDialog.SelectedPath, container.ToString());
                this.Dispose();
                this.configurationSuccess = true;
                this.configurationChanged = true;
            }
        }

        private String getParamNameByIndex(String actionName, int index)
        {
            foreach (ArrayList item in this.mTemplate)
            {
                if (item[0].ToString() == actionName)
                {
                    return item[index].ToString();
                }
            }

            return "";
        }

        private void ExportResult(String path, String result)
        {
            using (StreamWriter outputFile = new StreamWriter(path + @"\redshift_configuration_params.txt"))
            {
                outputFile.WriteLine(result.Replace(System.Environment.NewLine, ""));
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.configurationChanged = false;
            this.Dispose();
        }
    }
}
