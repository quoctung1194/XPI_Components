using MagicSoftware.Integration.UserComponents;
using System;

namespace SDK_REDSHIFT_IDS
{
    public class IdsRedshiftData
    {
        [Id(1)]
        [PrimitiveDataTypes(DataType.Blob)]
        [DisplayPropertyName("selectVariable")]
        [AllowEmptyExpression]
        [Out]
        public Variable selectVariable { get; set; }
        public IdsRedshiftData()
        {
            selectVariable = new Variable();
            selectVariable.SetValue("");
        }
    }
}