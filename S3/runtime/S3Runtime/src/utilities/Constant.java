package utilities;

public class Constant {

	// link to a AWS credentials file in user's local computer
	public static final String USER_AWS_CREDENTIALS_LOCATION = "src/main/resources/AwsCredentials.properties";
	// an access key
	public static final String SERVER_ACCESS_KEY = "accessKey";
	// a secret key
	public static final String SEEVER_SECRET_KEY = "secretKey";

	public static final String SUFFIX = "/";

	// a bucket name key on the AWS cloud
	public static final String BUCKET_NAME_ON_CLOUD = "bucketNameOnCloud";
	// a folder name key on the AWS cloud
	public static final String FOLDER_NAME_ON_CLOUD = "folderNameOnCloud";
	// a file name key on the AWS cloud
	public static final String FILE_NAME_ON_CLOUD = "fileNameOnCloud";
	// a file name key on the AWS cloud
	public static final String LOCAL_FILE_TO_UPLOAD = "localFileToUpload";
	// a local path to store a downloaded file on user's computer
	public static final String LOCAL_PATH_TO_STORE_FILE = "localpathToStoreFile";
	// a file path on the AWS S3 cloud
	public static final String FILE_PATH_ON_CLOUD = "filePathOnCloud";

	// a string key in Json input from UI built on C#
	public static final String AUTHENTICATION_KEY = "authenticate";
	public static final String ACTIONS_KEY = "actions";
	public static final String CLASS_NAME_KEY = "className";

	public static final String TAG_NAME = "ConfigurationContent";

}
