package handler;

import java.util.List;

import org.json.JSONObject;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3ObjectSummary;

import utilities.Constant;

public class DeletionHandler implements ActionHandler {

	// a bucket name on the AWS cloud
	private String mBucketName;
	// a folder name on the AWS cloud
	private String mFolderName;
	// a file path on cloud
	private String mFilePathOnCloud;
	// a S3 Amazon client
	private AmazonS3 mClient;

	public DeletionHandler(JSONObject action, AmazonS3 client) {
		if (action.has(Constant.BUCKET_NAME_ON_CLOUD))
			this.mBucketName = action.getString(Constant.BUCKET_NAME_ON_CLOUD);
		if (action.has(Constant.FOLDER_NAME_ON_CLOUD))
			this.mFolderName = action.getString(Constant.FOLDER_NAME_ON_CLOUD);
		if (action.has(Constant.FILE_PATH_ON_CLOUD))
			this.mFilePathOnCloud = action.getString(Constant.FILE_PATH_ON_CLOUD);
		this.mClient = client;
	}

	public String getFilePathOnCloud() {
		return mFilePathOnCloud;
	}

	public void setFilePathOnCloud(String mFilePathOnCloud) {
		this.mFilePathOnCloud = mFilePathOnCloud;
	}

	public String getBucketName() {
		return mBucketName;
	}

	public void setBucketName(String mBucketName) {
		this.mBucketName = mBucketName;
	}

	public String getFolderName() {
		return mFolderName;
	}

	public void setFolderName(String mFolderName) {
		this.mFolderName = mFolderName;
	}

	public AmazonS3 getClient() {
		return mClient;
	}

	public void setClient(AmazonS3 mClient) {
		this.mClient = mClient;
	}

	@Override
	public void act() {
		if (this.getBucketName().isEmpty() || this.getClient() == null) {
			return;
		}
		if (this.getFilePathOnCloud() != null && !this.getFilePathOnCloud().isEmpty()) {
			// we will delete a file iff a user specifies it
			deleteFile(this.getBucketName(), this.getFilePathOnCloud(), this.getClient());
		} else if (this.getFolderName() != null && !this.getFolderName().isEmpty()) {
			// if user does not specify a file to delete but he/she specifies a
			// folder, so we will delete that folder
			deleteFolder(mBucketName, mFolderName, mClient);
		} else {
			// finally will we delete a bucket if has
			this.deleteBucket(this.getBucketName(), this.getClient());
		}

	}

	/**
	 * This method first deletes all the files in given folder and than the
	 * folder itself
	 */
	public void deleteFolder(String bucketName, String folderName, AmazonS3 client) {
		List<S3ObjectSummary> fileList = client.listObjects(bucketName, folderName).getObjectSummaries();
		for (S3ObjectSummary file : fileList) {
			client.deleteObject(bucketName, file.getKey());
		}
		client.deleteObject(bucketName, folderName);
	}

	/**
	 * Delete a file on server
	 *
	 * @param bucketName
	 *            is a given bucket name
	 * @param filePathOnServer
	 *            is a file path on the AWS S3 server
	 * @param client
	 *            is a user
	 */
	public void deleteFile(String bucketName, String filePathOnServer, AmazonS3 client) {
		client.deleteObject(bucketName, filePathOnServer);
	}

	/**
	 * Delete a bucket
	 *
	 * @param bucketName
	 *            is a given bucket name
	 * @param client
	 *            is a user
	 */
	public void deleteBucket(String bucketName, AmazonS3 client) {
		// delete all objects in a given bucket
		ObjectListing objectListing = client.listObjects(new ListObjectsRequest().withBucketName(bucketName));
		for (S3ObjectSummary objectSummary : objectListing.getObjectSummaries()) {
			deleteFile(bucketName, objectSummary.getKey(), client);
        }

		// delete a bucket
		client.deleteBucket(bucketName);
	}

}
