package handler;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import org.json.JSONObject;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;

import utilities.Constant;

public class CreationHandler implements ActionHandler {

	// a bucket name on the AWS cloud
	private String mBucketName;
	// a folder name on the AWS cloud
	private String mFolderName;
	// a S3 Amazon client
	private AmazonS3 mClient;

	public CreationHandler(JSONObject action, AmazonS3 client) {
		if (action.has(Constant.BUCKET_NAME_ON_CLOUD))
			this.mBucketName = action.getString(Constant.BUCKET_NAME_ON_CLOUD);
		if (action.has(Constant.FOLDER_NAME_ON_CLOUD))
			this.mFolderName = action.getString(Constant.FOLDER_NAME_ON_CLOUD);
		this.mClient = client;
	}

	public String getBucketName() {
		return mBucketName;
	}

	public void setBucketName(String mBucketName) {
		this.mBucketName = mBucketName;
	}

	public String getFolderName() {
		return mFolderName;
	}

	public void setFolderName(String mFolderName) {
		this.mFolderName = mFolderName;
	}

	public AmazonS3 getClient() {
		return mClient;
	}

	public void setClient(AmazonS3 mClient) {
		this.mClient = mClient;
	}

	@Override
	public void act() {
		if (this.getBucketName().isEmpty() || this.getClient() == null) {
			return;
		}
		// we will create a bucket iff the Json input does not specify a folder
		// name to crrate
		if (this.getFolderName() == null || this.getFolderName().isEmpty()) {
			createBucket(mBucketName, mClient);
		} else {
			// we creare a folder if a user specifies it
			createFolder(mBucketName, mFolderName, mClient);
		}

	}

	/**
	 * Creates a folder in the given bucket
	 *
	 * @param bucketName
	 *            is a given bucket name
	 * @param folderName
	 *            is a given folder
	 * @param client
	 *            is a user
	 */
	public void createFolder(String bucketName, String folderName, AmazonS3 client) {
		// create meta-data for your folder and set content-length to 0
		ObjectMetadata metadata = new ObjectMetadata();
		metadata.setContentLength(0);
		// create empty content
		InputStream emptyContent = new ByteArrayInputStream(new byte[0]);
		// create a PutObjectRequest passing the folder name suffixed by /
		PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, folderName + utilities.Constant.SUFFIX,
				emptyContent, metadata);
		// send request to S3 to create folder
		client.putObject(putObjectRequest);
	}

	/**
	 * Create a new S3 bucket - Amazon S3 bucket names are globally unique,
	 * so once a bucket name has been taken by any user, you can't create
	 * another bucket with that same name.
	 *
	 * You can optionally specify a location for your bucket if you want to
	 * keep your data closer to your applications or users.
	 *
	 * @param bucketName
	 *            is a given bucket name
	 * @param client
	 *            is a user
	 */
	public void createBucket(String bucketName, AmazonS3 client) {
		client.createBucket(bucketName);
	}

}
