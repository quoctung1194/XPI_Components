package handler;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.json.JSONObject;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;

import utilities.Constant;
import utilities.FileHandlerUtils;

public class DownloadHandler implements ActionHandler {

	// a bucket name on the AWS cloud
	private String mBucketName;
	// a file name on the AWS cloud
	private String mFilePathOnCloud;
	// a S3 Amazon client
	private AmazonS3 mClient;
	// a local path to store
	private String mLocalPathToStore;

	public DownloadHandler(JSONObject obj, AmazonS3 client) {
		this.mBucketName = obj.getString(Constant.BUCKET_NAME_ON_CLOUD);
		this.mFilePathOnCloud = obj.getString(Constant.FILE_PATH_ON_CLOUD);
		this.mLocalPathToStore = obj.getString(Constant.LOCAL_PATH_TO_STORE_FILE);
		this.mClient = client;
	}

	public String getBucketName() {
		return mBucketName;
	}

	public String getLocalPathToStore() {
		return mLocalPathToStore;
	}

	public void setLocalPathToStore(String mLocalPathToStore) {
		this.mLocalPathToStore = mLocalPathToStore;
	}

	public void setBucketName(String mBucketName) {
		this.mBucketName = mBucketName;
	}

	public String getFilePathOnCloud() {
		return mFilePathOnCloud;
	}

	public void setFilePathOnCloud(String mFileName) {
		this.mFilePathOnCloud = mFileName;
	}

	public AmazonS3 getClient() {
		return mClient;
	}

	public void setClient(AmazonS3 mClient) {
		this.mClient = mClient;
	}

	@Override
	public void act() throws IOException {
		downloadFile(this.getBucketName(), this.getFilePathOnCloud(), this.getLocalPathToStore(), this.getClient());
	}

	/**
	 * Downloads all files in a given folder Note*: this function need to update
	 *
	 * @param bucketName
	 *            is a given bucket name
	 * @param folderName
	 *            is a given folder
	 * @param client
	 *            is a user
	 * @throws IOException
	 */
	public static void downloadAllFilesInGivenFolder(String bucketName, String folderName, String pathToSave,
			AmazonS3 client) throws IOException {
		List<S3ObjectSummary> fileList = client.listObjects(bucketName, folderName).getObjectSummaries();
		for (S3ObjectSummary file : fileList) {
			downloadFile(bucketName, file.getKey(), pathToSave, client);
		}
	}

	/**
	 * Downloads a file
	 *
	 * @param bucketName
	 *            is a given bucket name
	 * @param folderName
	 *            is a given folder
	 * @param client
	 *            is a user
	 * @throws IOException
	 */
	public static void downloadFile(String bucketName, String filePathOnCloud, String pathToSave, AmazonS3 client)
			throws IOException {
		// fetch a file on the S3 cloud
		S3Object fileOnCloud = client.getObject(new GetObjectRequest(bucketName, filePathOnCloud));
		// read it
		InputStream reader = new BufferedInputStream(fileOnCloud.getObjectContent());
		// write it
		FileHandlerUtils.writeFile(reader, pathToSave, filePathOnCloud);
	}

}
