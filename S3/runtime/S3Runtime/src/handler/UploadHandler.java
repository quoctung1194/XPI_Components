package handler;

import java.io.File;

import org.json.JSONObject;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;

import utilities.Constant;

public class UploadHandler implements ActionHandler {

	// a bucket name on the AWS cloud
	private String mBucketNameOnCloud;
	// a folder name on the AWS cloud
	private String mFolderNameOnCloud;
	// a local path of a file to upload
	private String mLocalPathOfFileToUpload;
	// a S3 Amazon client
	private AmazonS3 mClient;

	public UploadHandler(JSONObject obj, AmazonS3 client) {

		this.mBucketNameOnCloud = obj.getString(Constant.BUCKET_NAME_ON_CLOUD);
		this.mFolderNameOnCloud = obj.getString(Constant.FOLDER_NAME_ON_CLOUD);
		this.mLocalPathOfFileToUpload = obj.getString(Constant.LOCAL_FILE_TO_UPLOAD);
		this.mClient = client;
	}

	public String getBucketNameOnCloud() {
		return mBucketNameOnCloud;
	}

	public void setBucketNameOnCloud(String mBucketNameOnCloud) {
		this.mBucketNameOnCloud = mBucketNameOnCloud;
	}

	public String getFolderNameOnCloud() {
		return mFolderNameOnCloud;
	}

	public void setFolderNameOnCloud(String mFolderNameOnCloud) {
		this.mFolderNameOnCloud = mFolderNameOnCloud;
	}

	public String getLocalPathOfFile2Upload() {
		return mLocalPathOfFileToUpload;
	}

	public void setLocalPathOfFile2Upload(String mLocalPathOfFile2Upload) {
		this.mLocalPathOfFileToUpload = mLocalPathOfFile2Upload;
	}

	public AmazonS3 getClient() {
		return mClient;
	}

	public void setClient(AmazonS3 mClient) {
		this.mClient = mClient;
	}

	@Override
	public void act() {
		uploadFile(mBucketNameOnCloud, mFolderNameOnCloud, mLocalPathOfFileToUpload, mClient);
	}

	/**
	 * Upload a file
	 *
	 * @param bucketNameOnCloud
	 *            is a given bucket name
	 * @param folderNameOnCloud
	 *            is a given folder name on the cloud i.e.
	 *            "C:\\Users\\Public\\Pictures\\Sample
	 *            Pictures\\Tree_thumb.jpeg"
	 * @param localPathOfFile2Upload
	 *            is a local path of the file to upload onto the cloud
	 * @param client
	 *            is a user
	 */
	public void uploadFile(String bucketNameOnCloud, String folderNameOnCloud, String localPathOfFile2Upload,
			AmazonS3 client) {
		File file2Upload = new File(localPathOfFile2Upload);
		String fileNameOnCloud = folderNameOnCloud + utilities.Constant.SUFFIX + file2Upload.getName();
		// upload a file to a folder on the S3 cloud and set it to public
		client.putObject(new PutObjectRequest(bucketNameOnCloud, fileNameOnCloud, file2Upload)
				.withCannedAcl(CannedAccessControlList.PublicRead));
	}

}
