package gate.way;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.json.JSONArray;
import org.json.JSONObject;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;

import utilities.Constant;

/**
 *
 * @author Phuc Huynh
 *
 */
public class S3GateWay {

	/**
	 * Gets a client using a file
	 *
	 * @return client
	 */
	public static AmazonS3 getS3Client() throws FileNotFoundException, IOException {
		// credentials object identifying user for authentication
		// user must have AWSConnector and AmazonS3FullAccess
		Properties properties = new Properties();
		properties.load(new FileInputStream(Constant.USER_AWS_CREDENTIALS_LOCATION));
		BasicAWSCredentials credentials = new BasicAWSCredentials(properties.getProperty(Constant.SERVER_ACCESS_KEY),
				properties.getProperty(Constant.SEEVER_SECRET_KEY));

		// create a client connection based on credentials
		AmazonS3 s3client = AmazonS3ClientBuilder.standard()
				.withCredentials(new AWSStaticCredentialsProvider(credentials)).withRegion(Regions.AP_NORTHEAST_1)
				.build();

		return s3client;
	}

	/**
	 * Gets a client using plaintext keys
	 *
	 * @return client
	 */
	public static AmazonS3 getS3Client(String accessKey, String secretKey) throws FileNotFoundException, IOException {
		// credentials object identifying user for authentication
		// user must have AWSConnector and AmazonS3FullAccess
		BasicAWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);

		// create a client connection based on credentials
		AmazonS3 s3client = AmazonS3ClientBuilder.standard()
				.withCredentials(new AWSStaticCredentialsProvider(credentials)).withRegion(Regions.AP_NORTHEAST_1)
				.build();

		return s3client;
	}

	/**
	 * Gets a client
	 *
	 * @return client
	 */
	public static AmazonS3 getS3Client(JSONObject UIInput) throws FileNotFoundException, IOException {
		JSONArray authenticateArray = UIInput.getJSONArray(Constant.AUTHENTICATION_KEY);
		String accessKey = "";
		String secretKey = "";
		// get keys
		for (int i = 0; i < authenticateArray.length(); i++) {
			accessKey = authenticateArray.getJSONObject(i).getString(Constant.SERVER_ACCESS_KEY);
			secretKey = authenticateArray.getJSONObject(i).getString(Constant.SEEVER_SECRET_KEY);
		}

		return getS3Client(accessKey, secretKey);
	}
}
