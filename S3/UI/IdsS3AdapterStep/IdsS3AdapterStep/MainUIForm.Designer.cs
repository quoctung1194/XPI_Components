﻿namespace SDK_S3_IDS
{
    partial class MainUIForm
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>S
        /// <param name="disposing">マネージ リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnDel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.GridTask = new System.Windows.Forms.DataGridView();
            this.Value = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Param = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GridParam = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.publicKey = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.sceretKey = new System.Windows.Forms.TextBox();
            this.Task = new System.Windows.Forms.DataGridViewComboBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.GridTask)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridParam)).BeginInit();
            this.SuspendLayout();
            // 
            // btnDel
            // 
            this.btnDel.Location = new System.Drawing.Point(16, 545);
            this.btnDel.Name = "btnDel";
            this.btnDel.Size = new System.Drawing.Size(79, 35);
            this.btnDel.TabIndex = 3;
            this.btnDel.Text = "Remove";
            this.btnDel.UseVisualStyleBackColor = true;
            this.btnDel.Click += new System.EventHandler(this.BtnDelClick);
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(404, 545);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(79, 35);
            this.btnOK.TabIndex = 4;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.BtnOKClick);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(502, 545);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(79, 35);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // GridTask
            // 
            this.GridTask.AllowUserToDeleteRows = false;
            this.GridTask.AllowUserToResizeColumns = false;
            this.GridTask.AllowUserToResizeRows = false;
            this.GridTask.BackgroundColor = System.Drawing.SystemColors.Window;
            this.GridTask.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.GridTask.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Task});
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.NullValue = " ";
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.GridTask.DefaultCellStyle = dataGridViewCellStyle1;
            this.GridTask.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.GridTask.GridColor = System.Drawing.Color.White;
            this.GridTask.Location = new System.Drawing.Point(14, 105);
            this.GridTask.Name = "GridTask";
            this.GridTask.RowTemplate.Height = 21;
            this.GridTask.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GridTask.Size = new System.Drawing.Size(143, 424);
            this.GridTask.TabIndex = 1;
            this.GridTask.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridTaskCellValueChanged);
            this.GridTask.CurrentCellDirtyStateChanged += new System.EventHandler(this.GridTaskCurrentCellDirtyStateChanged);
            this.GridTask.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridTaskRowEnter);
            // 
            // Value
            // 
            this.Value.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Value.HeaderText = "Value";
            this.Value.Name = "Value";
            this.Value.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Param
            // 
            this.Param.FillWeight = 150F;
            this.Param.HeaderText = "Parameter Name";
            this.Param.Name = "Param";
            this.Param.ReadOnly = true;
            this.Param.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Param.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Param.Width = 150;
            // 
            // GridParam
            // 
            this.GridParam.AllowUserToAddRows = false;
            this.GridParam.AllowUserToDeleteRows = false;
            this.GridParam.AllowUserToResizeColumns = false;
            this.GridParam.AllowUserToResizeRows = false;
            this.GridParam.BackgroundColor = System.Drawing.Color.White;
            this.GridParam.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridParam.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Param,
            this.Value});
            this.GridParam.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.GridParam.GridColor = System.Drawing.Color.White;
            this.GridParam.Location = new System.Drawing.Point(204, 105);
            this.GridParam.Name = "GridParam";
            this.GridParam.RowHeadersVisible = false;
            this.GridParam.RowTemplate.Height = 21;
            this.GridParam.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GridParam.Size = new System.Drawing.Size(377, 424);
            this.GridParam.TabIndex = 0;
            this.GridParam.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridParamCellValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 12);
            this.label1.TabIndex = 6;
            this.label1.Text = "Public Key";
            // 
            // publicKey
            // 
            this.publicKey.Location = new System.Drawing.Point(84, 12);
            this.publicKey.Name = "publicKey";
            this.publicKey.Size = new System.Drawing.Size(239, 19);
            this.publicKey.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 12);
            this.label2.TabIndex = 8;
            this.label2.Text = "Sceret Key";
            // 
            // sceretKey
            // 
            this.sceretKey.Location = new System.Drawing.Point(84, 48);
            this.sceretKey.Name = "sceretKey";
            this.sceretKey.Size = new System.Drawing.Size(239, 19);
            this.sceretKey.TabIndex = 9;
            // 
            // Task
            // 
            this.Task.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Task.FillWeight = 130F;
            this.Task.HeaderText = "Task";
            this.Task.Items.AddRange(new object[] {
            Constant.CREATE_BUCKET,
            Constant.DELETE_BUCKET,
            Constant.CREATE_FOLDER,
            Constant.DELETE_FOLDER,
            Constant.DELETE_FILE,
            Constant.UPLOAD,
            Constant.DOWNLOAD});
            this.Task.Name = "Task";
            this.Task.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // MainUIForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(630, 594);
            this.Controls.Add(this.sceretKey);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.publicKey);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnDel);
            this.Controls.Add(this.GridTask);
            this.Controls.Add(this.GridParam);
            this.Name = "MainUIForm";
            this.Text = "S3 Connector";
            ((System.ComponentModel.ISupportInitialize)(this.GridTask)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridParam)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnDel;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.DataGridView GridTask;
        private System.Windows.Forms.DataGridViewTextBoxColumn Value;
        private System.Windows.Forms.DataGridViewTextBoxColumn Param;
        private System.Windows.Forms.DataGridView GridParam;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox publicKey;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox sceretKey;
        private System.Windows.Forms.DataGridViewComboBoxColumn Task;
    }
}

