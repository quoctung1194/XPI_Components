using MagicSoftware.Integration.UserComponents.Interfaces;
using MagicSoftware.Integration.UserComponents;
using System.ComponentModel.Composition;

namespace SDK_S3_IDS
{
    [Export(typeof(IUserComponent))]
    public class IdsS3AdapterStep : IUserComponent
    {
        private ISDKStudioUtils utils;
        public IdsS3AdapterStep()
        {
        }

        #region IUserComponent implementation
        public object CreateDataObject()
        {
            return new IdsS3Data();
        }

        public bool? Configure(ref object dataObject, ISDKStudioUtils utils,
        IReadOnlyResourceConfiguration resourceData,
        object navigateTo, out bool configurationChanged)
        {
            this.utils = utils;
            var form = new MainUIForm();
            form.ShowDialog();

            configurationChanged = form.isConfigurationChanged;

            return form.isConfigurationSuccess;
        }
        public SchemaInfo GetSchema()
        {
            return GetXMLSchemaConfiguration();
        }
        public ICheckerResult Check(ref object data,
        IReadOnlyResourceConfiguration resourceData)
        {
            return null;
        }
        public bool ValidateResource
        (IReadOnlyResourceConfiguration resouceData, out string errorMsg)
        {
            errorMsg = null;
            return true;
        }
        public void InvokeResourceHelper(string helperID,
        IResourceConfiguration resouceData)
        {
        }
        #endregion
        public SchemaInfo GetXMLSchemaConfiguration()
        {
            XMLSchemaInfo xmlSchemaInfoLocal = new XMLSchemaInfo();
            xmlSchemaInfoLocal.SchemaName = "XML_Schema_Name";
            xmlSchemaInfoLocal.AlwayCreateNodes = true;
            xmlSchemaInfoLocal.AppendData = false;
            xmlSchemaInfoLocal.DataDestinationType = 0;
            xmlSchemaInfoLocal.Description = "Description...";
            xmlSchemaInfoLocal.RecursionDepth = 3;
            xmlSchemaInfoLocal.XMLEncoding = XMLSchemaInfo.UTF_8;
            xmlSchemaInfoLocal.XMLValidation = false;
            xmlSchemaInfoLocal.XSDSchemaFilePath
            = utils.GetSystemProperty("ConnectorPath") + "\\xsd\\SDK.xsd";
            return xmlSchemaInfoLocal;
        }
    }
}
