using MagicSoftware.Integration.UserComponents;
using System;

namespace SDK_S3_IDS
{
    public class IdsS3Data
    {
        [Id(1)]
        [PrimitiveDataTypes(DataType.Blob)]
        [DisplayPropertyName("selectVariable")]
        [AllowEmptyExpression]
        [Out]
        public Variable selectVariable { get; set; }
        public IdsS3Data()
        {
            selectVariable = new Variable();
            selectVariable.SetValue("");
        }
    }
}