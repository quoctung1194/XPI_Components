﻿using System;

namespace SDK_S3_IDS
{
    class Constant
    {
        // an access key
        public const String SERVER_ACCESS_KEY = "accessKey";
        // a secret key
        public const String SEEVER_SECRET_KEY = "secretKey";

        /// <summary>
        ///  class names in java Runtime
        /// </summary>
        public const String CREATION_JAVA_RUNTIME_CLASS_NAME = "Creation";
        public const String DELETION_JAVA_RUNTIME_CLASS_NAME = "Deletion";
        public const String UPLOAD_JAVA_RUNTIME_CLASS_NAME = "Upload";
        public const String DOWNLOAD_JAVA_RUNTIME_CLASS_NAME = "Download";
        public const String REDSHIFT_COPY_JAVA_RUNTIME_CLASS_NAME = "RedShift";

        /// <summary>
        /// some keys of Json file between Java runtime and C# UI
        /// </summary>
        // a bucket name key on the AWS cloud
        public const String BUCKET_NAME_ON_CLOUD = "bucketNameOnCloud";
        // a folder name key on the AWS cloud
        public const String FOLDER_NAME_ON_CLOUD = "folderNameOnCloud";
        // a file name key on the AWS cloud
        public const String FILE_NAME_ON_CLOUD = "fileNameOnCloud";
        // a file name key on the AWS cloud
        public const String LOCAL_FILE_TO_UPLOAD = "localFileToUpload";
        // a local path to store a downloaded file on user's computer
        public const String LOCAL_PATH_TO_STORE_FILE = "localpathToStoreFile";
        //// a file path on the AWS S3 cloud
        public const String FILE_PATH_ON_CLOUD = "filePathOnCloud";
        // an authentication key in Json input from C# to Java
        public const String AUTHENTICATION_KEY = "authenticate";
        // an actions key in Json input from C# to Java
        public const String ACTIONS_KEY = "actions";
        // a class name key in Json input from C# to Java, 
        // we use it to explicitly call a handler respectively
        public const String CLASS_NAME_KEY = "className";



        // the options
        public const String DOWNLOAD = "Download";
        public const String UPLOAD = "Upload";
        public const String CREATE = "Create";
        public const String CREATE_BUCKET = "Create A Bucket";
        public const String CREATE_FOLDER = "Create A Folder";
        public const String DELETE = "Delete";
        public const String DELETE_BUCKET = "Delete A Bucket";
        public const String DELETE_FOLDER = "Delete A Folder";
        public const String DELETE_FILE = "Delete A File";


        public const String PARAM_NAME = "Parameter Name";
        public const String PARAM_VALUE = "Value";
        // package names of the Java Handler classes
        public const String JAVA_PACKAGE_NAME = "handler.";
        // postfix of handler classes
        public const String POSTFIX_OF_HANDLER_CLASSES = "Handler";
    }
}
